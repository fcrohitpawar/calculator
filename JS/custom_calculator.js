$(document).ready(function(){
 	$(".loan_input").bind("keyup", function (e) {
 		this.value = this.value.replace(/[^0-9]/g, '');
 		var id = $(this).attr('id');
 		custom_calculator()
 		main_calculator();
  	});

  	$(".loan_input").bind("change", function (e) {
	  	main_calculator();
	});
});


$('.numeric').on('input', function (event) { 
    this.value = this.value.replace(/[^0-9]/g, '');
});

function main_calculator(){
	var loan_amount = $('#loan_amount').val();
	var annual_interest_rate = $('#annual_interest_rate').val();
	var loan_period_in_years = $('#loan_period_in_years').val();
	var payment_interval = $('#payment_interval').val();
	var number_of_payments_per_year = $('#number_of_payments_per_year').val();
	var start_date_of_loan = $('#start_date_of_loan').val();
	var regular_extra_payments = $('#regular_extra_payments').val();

	console.log('loan_amount: ', loan_amount);
	console.log('annual_interest_rate: ', annual_interest_rate);
	console.log('loan_period_in_years: ', loan_period_in_years);
	console.log('payment_interval: ', payment_interval);
	console.log('number_of_payments_per_year: ', number_of_payments_per_year);
	console.log('start_date_of_loan: ', start_date_of_loan);
	console.log('regular_extra_payments: ', regular_extra_payments);

	var data = {};
	data.payment_interval = payment_interval;
	data.start_date_of_loan = start_date_of_loan;
	data.loan_period_in_years = loan_period_in_years;

	if(payment_interval != '' && start_date_of_loan != '' && loan_period_in_years!=''){
		calculate_payment_interval(data);	
	}
}

function calculate_payment_interval(data){
	var start_date_of_loan = new Date($("#start_date_of_loan").val());
	var dd = start_date_of_loan.getDate();
	var mm = start_date_of_loan.getMonth()+1; 
	var yyyy = start_date_of_loan.getFullYear();
	if(dd<10) {dd='0'+dd;} 
	if(mm<10) {mm='0'+mm;} 
	start_date_of_loan = dd+'/'+mm+'/'+yyyy;
	var endDate = ending_date(start_date_of_loan); 

	switch (data.payment_interval) {
		case 'Weekly':
			var dates = Weekly(start_date_of_loan, endDate);
			break;
		case 'Fortnightly':
		case 'Bi-weekly':
		case 'Semi-monthly':
		case 'Monthly':
			var dates = dateRange(start_date_of_loan, endDate);
			break;
		case 'Bi-monthly':
		case 'Quarterly':
		case 'Semi-annually':
		case 'Annually':
		default:
			return false;
	}
}


function ending_date(str){
    if( /^\d{2}\/\d{2}\/\d{4}$/i.test( str ) ) {
	    var parts = str.split("/");
	    var day = parts[0] && parseInt( parts[0], 10 );
	    var month = parts[1] && parseInt( parts[1], 10 );
	    var year = parts[2] && parseInt( parts[2], 10 );
	    var duration = parseInt( $("#loan_period_in_years").val(), 10);
	    if( day <= 31 && day >= 1 && month <= 12 && month >= 1 ) {
	        var expiryDate = new Date( year, month - 1, day );
	        expiryDate.setFullYear( expiryDate.getFullYear() + duration );
	        var day = ( '0' + expiryDate.getDate() ).slice( -2 );
	        var month = ( '0' + ( expiryDate.getMonth() + 1 ) ).slice( -2 );
	        var year = expiryDate.getFullYear();
	        return ''+day + "/" + month + "/" + year+'';
	    }
	}
}

function dateRange(startDate, endDate) {
  var start      = startDate.split("/");
  var end        = endDate.split("/");
  var startYear  = parseInt(start[2]);
  var endYear    = parseInt(end[2]);
  var dates      = [];

  for(var i = startYear; i <= endYear; i++) {
    var endMonth = i != endYear ? 11 : parseInt(end[1]) - 1;
    var startMon = i === startYear ? parseInt(start[1])-1 : 0;
    for(var j = startMon; j <= endMonth; j = j > 12 ? j % 12 || 11 : j+1) {
      var month = j+1;
      var displayMonth = month < 10 ? '0'+month : month;
      dates.push([start[0],displayMonth,i].join("/"));
    }
  }
  return dates;
}

function Weekly(startDate, endDate){
	var weeks = [];
	var startDate = moment(startDate).isoWeekday(8);
	if(startDate.date() == 8) {
	    startDate = startDate.isoWeekday(-6)
	}
	var today = moment(endDate).isoWeekday('Sunday');
	while(startDate.isBefore(today)) {
	  let startDateWeek = startDate.isoWeekday('Monday').format('DD-MM-YYYY');
	  let endDateWeek = startDate.isoWeekday('Sunday').format('DD-MM-YYYY');
	  startDate.add(7,'days');
	  weeks.push([startDateWeek]);
	}
	return weeks;
}

 function custom_calculator(){
        // var p = r = n = '';
        // var a = 0;
        // p = $('#loan_amount').val();
        // r = $('#annual_interest_rate').val();
        // n = $('#loan_period_in_years').val();

        // if(r != ''){
        //     var r  = ((r*100)/12);
        // }

        // if(n != ''){
        //     var n  = (n*12);
        // }


        // if(p != '' && r != '' && n != '' ){
        //     var flg = 1+r;
        //     var flg2 = Math.pow(flg, n);
        //     a = (p)*(r*((flg2)/(flg2-1)));
        // }

        var investment = 100000;
		var annualRate = 6;
		var monthlyRate = annualRate / 12 / 100;
		var years = 3;
		var months = years * 12;
		var futureValue = 0;

		// for ( i = 1; i <= months; i++ ) {
		//     futureValue = (futureValue + investment) * (1 + monthlyRate);
		//     alert(futureValue);
		// }
		futureValue = investment * (Math.pow(1 + monthlyRate, months) - 1) / monthlyRate;
		    alert(futureValue);
    }